# Ud3_Ejemplo9
_Ejemplo 9 de la Unidad 3._ 

Vamos a crear un _Floating Action Button (FAB)_ que cuando se pulse aparezca un mensaje por pantalla. 

Para ello primero lo incluimos en nuestro _layout_.

_activity_main.xml_:
```html
...
        <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:id="@+id/fab"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="end|bottom"
        android:layout_marginEnd="32dp"
        android:layout_marginRight="32dp"
        android:layout_marginBottom="32dp"
        android:src="@drawable/estrella"
        app:tint="@android:color/white"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent" />

</androidx.constraintlayout.widget.ConstraintLayout>
```
Y luego en la clase _MainActivity_ lo buscamos y le asignamos el evento asociado.

_MainActivity.kt_:
```java
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Accedemos al FAB y le asignamos el Click Listener
        binding.fab.setOnClickListener {
            Toast.makeText(this, "FAB", Toast.LENGTH_SHORT).show()
        }
    }
}
```
_Imagen obtenida de material.io/tools/icons (Licencia Apache 2.0)_

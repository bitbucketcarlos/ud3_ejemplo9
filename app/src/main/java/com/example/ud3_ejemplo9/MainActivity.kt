package com.example.ud3_ejemplo9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.ud3_ejemplo9.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Accedemos al FAB y le asignamos el Click Listener
        binding.fab.setOnClickListener {
            Toast.makeText(this, "FAB", Toast.LENGTH_SHORT).show()
        }
    }
}